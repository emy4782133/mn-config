# Morning News - Config

Ce repos comprend tous les fichiers de configuration pour le projet Morning News

## Déploiement Back et Front Preprod

Le fichier docker-compose.yml permet de mettre en place l'environnement de preproduction avec le front, back et la bdd. Il comprend deux images personalisées:
- "registry.gitlab.com/mngroup1/morning-new" pour le back
- "registry.gitlab.com/mngroup1/morning-news-front" pour le front

## Infra Deployement
Terraform x2
Fichier de déploiement des instances sur AWS maint.tf
Instance:
- Prod -> linode
- Préprod
- Monitoring
Le groupe de sécurité:
- Autorise connexion HTTP/HTTPS à tous
- SSH que pour les membres du groupe
IP élastique automatique
Ansible:
User-ansible.yml

## Déploiement du backend K8S Prod
Deployment et service:
- mn-backend-svc-dpl.yml
- mn-backend-ingress.yaml
- acme-issuer-prod.yml (obtention des certificats TLS let's encrypt)
Modus opérandis pour obtention des certificats:
- Installer helm et suivre les étapes:
https://www.linode.com/docs/guides/how-to-configure-load-balancing-with-tls-encryption-on-a-kubernetes-cluster/

Secret:
- connection Gilab
- ConfigMap

## Déploiement du frontend K8S Prod
Deployment et service:
- mn-frontend-svc-dpl.yml

## Configuration des serveurs

### Configuration de docker et docker-compose
- ansible-docker.yml

### Configuration du monitoring
- ansible-monitoring.yml : Déploiement et configuration de Prometheus / Node Exporter / Grafana
- prometheus.yml : Configuration de Prometheus
- datasources.yml : Configuration de Grafana
- blackbox.yml: Configuration blacbox
- blackbox-install.yml: Installation de l'exporter blackbox

### Configuration du backup de base de données
- ansible-aws-s3.yml : Déploiment d'un bucket AWS S3 pour stocker les fichiers de sauvegarde et configuration de AWS CLI pour communiquer avec celui-ci.
- ansible-backup.yml : Configuration de mongodump pour faire les backups de BDD, mise en place de l'arborescence et configuration du script Bash qui exécute le backup et la copie vers le S3 dans la crontab (exécution journalière).
- backup_database_mongo_cloud.sh : Fichier Bash correspondant aux commandes à passer.
- lifecycleS3Glacier.json : Fichier de configuration pour transformer un S3 en S3 glacier.

### Configuration sécurité
- ansible pour configuration SSH:
    -  port SSH customisé
    -  connexion via SSH sans mot de passe
    -  connexion root désactivé
- ansible Ufw:
    -   ansible installation et règle firewall prod
    -   ansible installation et règle firewall préprod
    -   ansible installation et règle firewall monitoring
- exemple synthax hosts pour inventaire ansible    


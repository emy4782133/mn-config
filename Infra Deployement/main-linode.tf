terraform {
  required_providers {
    linode = {
      source  = "linode/linode"
      version = "1.27.1"
    }
  }
}

provider "linode" {
  token = "32422f3c822934348df0c6311806cac5f8a51c0fef19c97c88059f4c162"
}


resource "linode_instance" "test-ansible2" {
  image           = "linode/debian11"
  label           = "Terraform-instance"
  group           = "Terraform"
  region          = "fr-par"
  type            = "g6-standard-2"
  authorized_keys = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCqS1FO7t0mslBbKO0B+a7IkNf9V+3UVYl+L3VyTX8nzB5SYTSn0ZuM7S/m6hNm/GSd3mm/lSA8dizqmBIGbb4z3ZZGceLEXRKYNO6sHQLeZRrfTyCfkYFeElbS6vJBuxjA/5S+vyILdlv1yDpMVF87I++2LB0j78m4RKyTMM59j/r2HYT+WzRJbWu9TYLS6htSmF4Ar1DDKtKVKiLpkisF2Wp7hufdixpoqJOv0K3k+po4AXbUtDvHAet63n8FANsYcf794OHEOoQjxx9F5EfO+6OHecHuEmawas5JQ3cN6vJ49IgDdtyb1iQxz7rpQDRhsZlS2EA+8gRCnZ0Jowoe/ajv6l6rktiTBZUMEpk1XDFUNDYocp5ccwyqfw7gTkos4puTuORs7tB1SX0b96SmmWip0uycxN3Z4qGVoxCuRvnaUfAs/sJt7f4nrBHH31WI7/XGEXCRnx4Lqs/0fqGB5ufIoj1+CA7BA1v6NJ3pbiAqzQZWQniMf3g8eU= engineer@localhost.localdomain"]
  root_pass       = "motdepasse"
}

//Liste instance linode
//data "linode_instance_types" "all-types" {}
//output "type_ids" {
// value = data.linode_instance_types.all-types.types.*.id
//}
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"

    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "eu-north-1"
}

resource "aws_key_pair" "morning-news" {
  key_name   = "mn-aws"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQD1fBmJszBcSIi7AOIJPFum1VOb1XyuF9S/YyEWoelmz4oXg3HJINss+10KXgC0SHbMQUt0tFGDaT90+H3/qfEmDC/tB2nRoY6GSUNRj1F/Dpc5U8KF0LyqhSv4dSCSQD3YoO6LBwFVjxWuel8Ce2b87tRtrYdYELSG1Lv4BS0MO4D7wubGdpKu8VhrIeIcz3x4RgUoWI3j/bDOIwCsM1JsBAerPVttBzgTshPSKd6JeRHEgZO3rQIgnPrxXNySPdFhX0rTym4haqTG16Pij4YQWVEVNkzqU46rdEgwKzrDNUnirFYvuYuQ8qmuib7RDKr2q38ePI0DbV2n5+iS4Yu1aN8NIsVWyLUFb4j1VZ+/P0lMH+b/0EDRe3na0dNs615grZTDMepLzBd7KPyQ6oEFKwytPhyPUEd6+Xax1O2PYyC2cCWIGoDfQx/tDWaQDyUWwbx8CuhS0LZhQ5Pm9wFQaxGEPmHVpj6BAAnKEBGEUNm8zUs/PbPe8xQ8CD7HI/U= engineer@localhost.localdomain"
}

# Define sécurity Group
resource "aws_security_group" "morning-news-sg" {
  name = "morning-sec"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # HTTP
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["194.233.172.178/32", "192.168.1.133/32", "141.94.138.152/32", "194.195.240.151/32"]
  }
  # SSH custom
  ingress {
    from_port   = 1988
    to_port     = 1988
    protocol    = "tcp"
    cidr_blocks = ["194.233.172.178/32", "192.168.1.133/32", "141.94.138.152/32", "194.195.240.151/32"]
  }
  # Prometheus
  ingress {
    from_port   = 9090
    to_port     = 9090
    protocol    = "tcp"
    cidr_blocks = ["194.233.172.178/32", "192.168.1.133/32", "141.94.138.152/32", "194.195.240.151/32"]
  }

  # Grafana
  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["194.233.172.178/32", "192.168.1.133/32", "141.94.138.152/32", "194.195.240.151/32"]
  }
  # frontend
  ingress {
    from_port   = 3001
    to_port     = 3001
    protocol    = "tcp"
    cidr_blocks = ["194.233.172.178/32", "192.168.1.133/32", "141.94.138.152/32", "194.195.240.151/32"]
  }
  # Mongodb
  ingress {
    from_port   = 9216
    to_port     = 9216
    protocol    = "tcp"
    cidr_blocks = ["194.233.172.178/32", "192.168.1.133/32", "141.94.138.152/32", "194.195.240.151/32"]
  }
}

resource "aws_instance" "mn-monitoring" {
  ami             = "ami-0506d6d51f1916a96"
  instance_type   = "t3.micro"
  key_name        = "mn-aws"
  security_groups = ["morning-sec"]

  user_data = <<-EOF
    #!/bin/bash
    sudo apt update && sudo apt upgrade
    sudo apt install fail2ban -y
  EOF 

  ebs_block_device {
    device_name = "/dev/xvda"
    volume_size = 10
    volume_type = "gp2"
  }

  tags = {
    Name = "mn-monitoring"
  }
}


resource "aws_instance" "mn-preprod" {
  ami             = "ami-0506d6d51f1916a96"
  instance_type   = "t3.micro"
  key_name        = "mn-aws"
  security_groups = ["morning-sec"]

  user_data = <<-EOF
    #!/bin/bash
    sudo apt update && sudo apt upgrade
    sudo apt install fail2ban -y
  EOF 

  ebs_block_device {
    device_name = "/dev/xvda"
    volume_size = 20
    volume_type = "gp2"
  }

  tags = {
    Name = "mn-preprod"
  }
}


#Create an Elastic IP

resource "aws_eip" "mn-monitoring-eip" {
  vpc = true
}

# associate the elastic ip
resource "aws_eip_association" "mn-monitoring-eip-associate" {
  instance_id   = aws_instance.mn-monitoring.id
  allocation_id = aws_eip.mn-monitoring-eip.id
}

resource "aws_eip" "mn-preprod-eip" {
  vpc = true
}

# associate the elastic ip
resource "aws_eip_association" "mn-preprod-eip-associate" {
  instance_id   = aws_instance.mn-preprod.id
  allocation_id = aws_eip.mn-preprod-eip.id
}
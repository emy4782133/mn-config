#!/bin/bash

# MongoDB connection parameters
USER="admin"
PASSWORD="ihatemysql"
DATABASE="morningnews1"
HOST=".f7g5in2.mongodb.net/"
PARAMETERS="?retryWrites=true&w=majority"

# Backup parameters
BACKUP_PATH="/root/backup_mongodb/files"
BACKUP_NAME="backup-$(date +'%Y-%m-%d')"

# S3 bucket
S3_BUCKET_NAME="morningnewsemy"
AWS="/usr/local/bin/aws"


# Use mongodump to create a backup
mongodump --uri="mongodb+srv://$USER:$PASSWORD@$DATABASE$HOST$PARAMETERS" --gzip --archive="$BACKUP_PATH/$BACKUP_NAME.gz"

# Collect the return code of the last command
return_code=$?

# Check the return code
if [ $return_code -eq 0 ]; then

  sleep 10

  # Copy tarball gzip to S3 bucket
  $AWS s3 cp "$BACKUP_PATH/$BACKUP_NAME.gz" "s3://$S3_BUCKET_NAME/$BACKUP_NAME.gz"
fi

